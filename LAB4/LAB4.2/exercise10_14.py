#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=broad-except

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB4.2

Date:
    March 2020

Main script
"""

import unittest
import time
import uuid
import os

from ddt import ddt, file_data

class MyPowerList:
    """
    Class that provide a abstraction from list
    """

    def __init__(self):
        self.__elements = []

    def add_element(self, new_element):
        """ add """
        self.__elements.append(new_element)

    def remove_element(self, position):
        """ remove """
        self.__elements.pop(position)

    def get_elements(self):
        """ get """
        return self.__elements

    def get_elements_sorted(self):
        """ sort """

        sorted_list = list(self.__elements)
        lenght = len(sorted_list)

        for i in range(0, lenght):
            for j in range(0, lenght):
                if sorted_list[i] < sorted_list[j]:
                    tmp = sorted_list[i]
                    sorted_list[i] = sorted_list[j]
                    sorted_list[j] = tmp

        return sorted_list

    def left_merge(self, left_list):
        """ left_merge """
        self.__elements = left_list + self.__elements

    def right_merge(self, right_list):
        """ right_merge """
        self.__elements = self.__elements + right_list

    def save_to_file(self, filename):
        """ save file """
        with open(filename, "w") as _file:
            _file.write("\n".join(str(self.__elements)))

    def read_from_file(self, filename):
        """ read file """
        with open(filename, "r") as _file:
            self.__elements = list(map(lambda x: x.strip(), _file.readlines()))


@ddt
class MyTestCase(unittest.TestCase):
    """ Test Class """

    def general_flow(self, command_stream):
        """ general purpose test """

        os.system("mkdir -p tmpdata")
        _directory = "tmpdata/{0}".format(uuid.uuid1())

        _plist = MyPowerList()

        for command in command_stream:
            if command["cmd"] == "add":
                _plist.add_element(command["element"])

            elif command["cmd"] == "remove":
                _plist.remove_element(command["element"])

            elif command["cmd"] == "get":
                _elements = _plist.get_elements()
                self.assertEqual(_elements, command["element"])

            elif command["cmd"] == "sort":
                _elements = _plist.get_elements_sorted()
                self.assertEqual(_elements, command["element"])

            elif command["cmd"] == "right":
                _plist.right_merge(command["element"])

            elif command["cmd"] == "left":
                _plist.left_merge(command["element"])

        _plist.save_to_file(_directory)
        _plist.read_from_file(_directory)


    @file_data("resources/testdata_10_b.json")
    def test_boundary(self, command_stream):
        """ boundary """
        self.general_flow(command_stream)

    def test_inverse(self):
        """ invert """
        # No invert method to this class
        pass

    def test_cross(self):
        """ cross """
        # No public class with same methods
        pass

    @file_data("resources/testdata_10_e.json")
    def test_exception(self, command_stream, exception):
        """ exception """

        exception_name = ""
        try:
            self.general_flow(command_stream)
        except Exception as exp:
            exception_name = type(exp).__name__
        self.assertEqual(exception_name, exception)

    @file_data("resources/testdata_10_b.json")
    def test_performance(self, command_stream):
        """ performance """

        _start_time = time.time()
        self.general_flow(command_stream)
        _end_time = time.time()
        _total_time = _end_time - _start_time

        self.assertTrue(_total_time < 1)


if __name__ == "__main__":
    unittest.main()
