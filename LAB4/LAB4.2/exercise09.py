#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=broad-except,too-few-public-methods

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB4.2

Date:
    March 2020

Main script
"""

import time
import unittest

from ddt import ddt, file_data


class RomanConverter:
    """  Roman number converter """

    def __init__(self):
        self.__rules = {
            "M": 1000,
            "CM": 900,
            "D": 500,
            "CD": 400,
            "C": 100,
            "XC": 90,
            "L": 50,
            "XL": 40,
            "X": 10,
            "IX": 9,
            "V": 5,
            "IV": 4,
            "I": 1
        }

    def dec_to_rom(self, decimal):
        """
        Convert decimal number to roman number
        """

        if decimal >= 1000000:
            raise ArithmeticError

        if decimal >= 4000:

            million_part = int(str(decimal)[:-3])
            decimal_part = int(str(decimal)[-3:])

            million_part = self.dec_to_rom(million_part)
            million_part = list(map("*{}".format, list(million_part)))
            million_part = "".join(million_part)

            decimal_part = self.dec_to_rom(decimal_part)

            return million_part + decimal_part

        else:

            for letter, decimal_value in list(self.__rules.items()):
                if decimal >= decimal_value:
                    return letter + self.dec_to_rom(decimal - decimal_value)

        return ""


@ddt
class MyTestCase(unittest.TestCase):
    """ Test Class """

    def validate_roman(self, decimal, roman):
        """ validate roman number """
        _converter = RomanConverter()
        _new_roman = _converter.dec_to_rom(decimal)
        self.assertEqual(roman, _new_roman)

    @file_data("resources/testdata_09_b.json")
    def test_boundary(self, decimal, roman):
        """ boundary """
        self.validate_roman(decimal, roman)

    def test_inverse(self):
        """ invert """
        # No invert method to this class
        pass

    def test_cross(self):
        """ cross """
        # No cross library

    @file_data("resources/testdata_09_e.json")
    def test_exception(self, decimal, roman, exception):
        """ exception """

        exception_name = ""
        try:
            self.validate_roman(decimal, roman)
        except Exception as exp:
            exception_name = type(exp).__name__
        self.assertEqual(exception_name, exception)

    @file_data("resources/testdata_09_b.json")
    def test_performance(self, decimal, roman):
        """ performance """

        _start_time = time.time()
        self.validate_roman(decimal, roman)
        _end_time = time.time()
        _total_time = _end_time - _start_time

        self.assertTrue(_total_time < 1)


if __name__ == "__main__":
    unittest.main()
