import time
import threading
from abc import ABC, abstractmethod

class AbstractActiveObject(ABC):

    @abstractmethod
    def get_value(self):
        pass

    @abstractmethod
    def finnish(self):
        pass

    @abstractmethod
    def do_something(self):
        pass

    @abstractmethod
    def do_other_thing(self):
        pass

class MyActiveObject(AbstractActiveObject):

    def __init__(self):
        self.val = 0.0
        self.dispatch_list = []
        self.scheduler = None
        self.end = False

        self.init_scheduler()

    def get_value(self):
        return self.val

    def finnish(self):
        self.end = True

    def init_scheduler(self):
        self.scheduler = threading.Thread(target=self._check_tasks)
        self.scheduler.start()

    def _check_tasks(self):
        while not self.end:
            if self.dispatch_list:
                task = self.dispatch_list.pop(0)
                task.start()
                task.join()
            time.sleep(1)

    def do_something(self):
        thread = threading.Thread(target=self._do_something_async)
        self.dispatch_list.append(thread)

    def _do_something_async(self):
        print("change value to 1.0")
        self.val = 1.0

    def do_other_thing(self):
        thread = threading.Thread(target=self._do_other_async)
        self.dispatch_list.append(thread)

    def _do_other_async(self):
        print("change value to 2.0")
        self.val = 2.0

def main():
    active_object = MyActiveObject()

    active_object.do_something()
    active_object.do_other_thing()

    active_object.do_something()
    active_object.do_other_thing()

    print(active_object.get_value())
    time.sleep(5)
    print(active_object.get_value())

    active_object.finnish()

if __name__ == "__main__":
    main()