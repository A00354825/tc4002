#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=broad-except

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB3.1.18

Date:
    February 2020

Main script
"""

import unittest
import math
import time
from ddt import ddt, file_data

@ddt
class MyTestCase(unittest.TestCase):
    """ Test Class """

    @staticmethod
    def pow(base, exponent):
        """ pow """
        return pow(base, exponent)

    @staticmethod
    def inverse_pow(result, exponent):
        """ inverse pow """
        _base_pow = pow(abs(result), 1/exponent)
        return _base_pow

    @file_data("resources/testdata_18_b.json")
    def test_boundary(self, base, exponent, result):
        """ Test boundary """
        _res = math.pow(base, exponent)
        self.assertEqual(_res, result)

    @file_data("resources/testdata_18_b.json")
    def test_inverse(self, base, exponent, result):
        """ Test inverse """
        _res = MyTestCase.inverse_pow(result, exponent)
        self.assertEqual(_res, abs(base))

    @file_data("resources/testdata_18_b.json")
    def test_cross_check(self, base, exponent, result):
        """ Test cross check """
        _res1 = math.pow(base, exponent)
        _res2 = MyTestCase.pow(base, exponent)
        self.assertEqual(_res1, _res2)
        self.assertEqual(_res2, result)

    @file_data("resources/testdata_18_e.json")
    def test_error_condition(self, base, exponent, exception_name):
        """ Test error """
        try:
            math.pow(base, exponent)
            raise Exception
        except Exception as exp:
            self.assertEqual(type(exp).__name__, exception_name)

    @file_data("resources/testdata_18_b.json")
    def test_performance(self, base, exponent, result):
        """ test performance """
        _start_time = time.time()
        _res = math.pow(base, exponent)
        self.assertEqual(_res, result)
        _end_time = time.time()

        _total_time = _end_time - _start_time
        self.assertTrue(_total_time < 1)

if __name__ == "__main__":
    unittest.main()
