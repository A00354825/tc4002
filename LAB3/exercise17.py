#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=broad-except

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB3.1.17

Date:
    February 2020

Main script
"""

import unittest
import math
import time
from ddt import ddt, file_data

@ddt
class MyTestCase(unittest.TestCase):
    """ Test Class """

    @staticmethod
    def factorial(num):
        """ factorial """
        factorial = 1
        for i in range(1, num+1):
            factorial *= i
        return factorial

    @staticmethod
    def inverse_factorial(num):
        """ inverse factorial """
        factorial = 1
        factor = 0

        while True:
            factor += 1
            factorial *= factor

            if factorial >= num:
                break

        return factor

    @file_data("resources/testdata_17_b.json")
    def test_boundary(self, input_value, output_value):
        """ Test boundary """
        _res = math.factorial(input_value)
        self.assertEqual(_res, output_value)

    @file_data("resources/testdata_17_b.json")
    def test_inverse(self, input_value, output_value):
        """ Test inverse """
        _res = MyTestCase.inverse_factorial(output_value)
        self.assertEqual(_res, input_value)

    @file_data("resources/testdata_17_b.json")
    def test_cross_check(self, input_value, output_value):
        """ Test cross check """
        _res1 = math.factorial(input_value)
        _res2 = MyTestCase.factorial(input_value)
        self.assertEqual(_res1, _res2)
        self.assertEqual(_res2, output_value)

    @file_data("resources/testdata_17_e.json")
    def test_error_condition(self, input_value, exception_name):
        """ Test error """
        try:
            math.factorial(input_value)
            raise Exception
        except Exception as exp:
            self.assertEqual(type(exp).__name__, exception_name)

    @file_data("resources/testdata_17_b.json")
    def test_performance(self, input_value, output_value):
        """ test performance """
        _start_time = time.time()
        _res = math.factorial(input_value)
        self.assertEqual(_res, output_value)
        _end_time = time.time()

        _total_time = _end_time - _start_time
        self.assertTrue(_total_time < 1)


if __name__ == "__main__":
    unittest.main()
