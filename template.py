#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

B
Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB

Date:
    March 2020

Main script
"""

import unittest

from ddt import ddt, file_data

class DummyClass:
    """ Class Doc """

    def __init__(self):
        # init the class
        pass

    def one(self):
        """ one """
        pass

    def two(self):
        """ two """
        pass

@ddt
class MyTestCase(unittest.TestCase):
    """ Test Class """

    @file_data("resources/testdata__b.json")
    def test_boundary(self):
        """ boundary """

    @file_data("resources/testdata__b.json")
    def test_inverse(self):
        """ inverse """

    @file_data("resources/testdata__b.json")
    def test_cross(self):
        """ cross """

    @file_data("resources/testdata__e.json")
    def test_exception(self, exception):
        """ exception """

        _exception_name = ""
        try:
            # TODO
        except Exception as _exp:
            _exception_name = type(exp).__name__
        self.assertEqual(_exception_name, exception)

    @file_data("resources/testdata__b.json")
    def test_performance(self):
        """ performance """


if __name__ == "__main__":
    unittest.main()
