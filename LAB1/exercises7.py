#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB1, Ex7

Date:
    February 2020

Main script
"""

import random

def generate_password():
    """
    Write a password generator function in Python
    Be creative with how you generate passwords
    """

    password_len = random.randint(8, 16)
    new_password = ""

    for _ in range(0, password_len):
        char = chr(random.randint(32, 126))
        new_password += char

    return new_password

def generator():
    """
    Write a password generator function in Python
    """

    again = True
    while again:

        opt = input("Want a new password? [Y/N]: ")
        if opt == "Y":
            new = generate_password()
            print("Your new password is")
            print(new)
        else:
            again = False

if __name__ == "__main__":
    generator()
