#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB1, Ex5

Date:
    February 2020

Main script
"""

def fibonnaci(number_list, current_iter, max_iter):
    """
    Calculate fibonnaci
    """

    if current_iter <= max_iter:

        if current_iter == 1 or current_iter == 2:
            number_list.append(1)
        else:
            next_number = number_list[-1] + number_list[-2]
            number_list.append(next_number)

        fibonnaci(number_list, current_iter + 1, max_iter)

def run_fibonnaci(seq_size):
    """
    Fibonnaci boostrap
    """

    fibonnaci_seq = []
    try:
        fibonnaci(fibonnaci_seq, 1, seq_size)
    except RecursionError:
        print("Number too big to calculate Fibonnaci")
    except ValueError:
        print("Next time please insert a number")
    return fibonnaci_seq

def main():
    """
    Write a function that receives as parameters
    how many Fibonnaci numbers to generate and then generates them.
    """
    try:
        _max_size = int(input("Insert size of the fibonnaci seq: "))
        _seq = run_fibonnaci(_max_size)

        print("The Fibonacci seq is: {0}".format(_seq))
    except ValueError:
        print("Next time please insert a number")

if __name__ == "__main__":
    main()
