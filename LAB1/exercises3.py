#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB1, Ex3

Date:
    February 2020

Main script
"""

def approach1(input_list):
    """
    Write one function using a loop and constructing a list
    """

    if not isinstance(input_list, list):
        print("*WARN: input_list is not a list")
        return []

    new_list = []
    for element in input_list:
        if element not in new_list:
            new_list.append(element)

    return list(sorted(new_list))

def approach2(input_list):
    """
    Write one function using set
    """

    if not isinstance(input_list, list):
        print("*WARN: input_list is not a list")
        return []

    return list(sorted(list(set(input_list))))

def request_text():
    """
    Write a program (function!) that takes a list and returns
    a new list that contains all the elements of the
    first list minus all the duplicates
    """

    input_elements = input("Insert a text divided by spaces: ")
    input_elements = str(input_elements).split(" ")

    approach1_result = approach1(input_elements)
    approach2_result = approach2(input_elements)

    print("Approach1 result: {0}".format(approach1_result))
    print("Approach2 result: {0}".format(approach2_result))

if __name__ == "__main__":
    request_text()
