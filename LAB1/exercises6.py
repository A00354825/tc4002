#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB1, Ex6

Date:
    February 2020

Main script
"""

def fibonnaci_fx(number_list, current_iter, max_iter):
    """
    Calculate fibonnaci
    """

    if current_iter <= max_iter:

        if current_iter == 1 or current_iter == 2:
            number_list.append(1)
        else:
            next_number = number_list[-1] + number_list[-2]
            number_list.append(next_number)

        fibonnaci_fx(number_list, current_iter + 1, max_iter)

def run_fibonnaci(seq_size):
    """
    Fibonnaci boostrap
    """

    fibonnaci_seq = []
    try:
        fibonnaci_fx(fibonnaci_seq, 1, seq_size)
    except RecursionError:
        print("Number too big to calculate Fibonnaci")
    except ValueError:
        print("Next time please insert a number")
    return fibonnaci_seq

def validate_sequence(faker_seq):
    """
    Validate fibonnaci
    """
    _real_seq = run_fibonnaci(len(faker_seq))

    return faker_seq == _real_seq

def main():
    """
    Write a function that evaluates if a given list satisfy
    Fibonacci sequence returning true or false
    if the list satisfy the criteria
    """
    try:
        _seq = input("Insert a fibonnaci sequence: ")
        _seq = list(map(int, _seq.split(" ")))

        if validate_sequence(_seq):
            print("The input seq is Fibonnaci")
        else:
            print("The input seq not is Fibonnaci")

    except ValueError:
        print("Next time please insert a number")

if __name__ == "__main__":
    main()
