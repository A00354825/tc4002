#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB1, Ex2

Date:
    February 2020

Main script
"""

import random

def game():
    """
    Generate a random number between 1 and 9 (including 1 and 9).
    Ask the user to guess the number then tell them whether
    they guessed too low, too high, or exactly right
    Keep the game going until the user types “exit”
    Keep track of how many guesses the user has taken
    """

    while True:

        print("*")
        print("Welcome to random number")
        print("Insert 'exit' to close the program")
        print("*")

        tries = 0
        rand = random.randint(1, 9)
        current = 0

        while current != rand:

            try:
                inp = input("Insert a number: ")

                if inp == "exit":
                    print("The number was: {0}".format(rand))
                    return

                current = int(inp)
                if current > rand:
                    print("The number is bigger than the random")
                    tries += 1
                elif current < rand:
                    print("The number is lower than the random")
                    tries += 1
                else:
                    print("Good job, number found")
                    print("{0} tries".format(tries))

            except ValueError:
                print("Please insert a number the next time")

if __name__ == "__main__":
    game()
