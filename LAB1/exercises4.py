#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
   LAB1, Ex4

Date:
    February 2020

Main script
"""

from math import sqrt

def mean_fx(list_number):
    """
    Calculate mean
    """

    if not list_number:
        raise ValueError("No elements")

    sumatory = 0
    for number in list_number:
        sumatory += number

    return float(sumatory)/float(len(list_number))

def std_deviation_fx(list_number):
    """
    Calculate standard deviasion
    """

    if not list_number:
        raise ValueError("No elements")

    _mean = mean_fx(list_number)

    variance = 0
    for number in list_number:
        tmp = _mean - number
        tmp = tmp * tmp
        variance += tmp

    variance = variance/float(len(list_number))

    return sqrt(variance)


def requests_numbers():
    """
    Write a function that computes the standard deviation
    for a set of numbers coming from a list.
    Do not use any math module, compute the algorithm
    """

    list_element = input("Insert numbers divided by spaces: ")

    try:
        list_element = list(map(float, list_element.split(" ")))
        std = std_deviation_fx(list_element)
        print("The standard division is: {0}".format(std))

    except ValueError:
        print("Next time please insert a number")


if __name__ == "__main__":
    requests_numbers()
