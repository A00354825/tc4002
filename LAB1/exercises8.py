#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
   LAB1, Ex8

Date:
    February 2020

Main script
"""

import math

def median_fx(list_number):
    """
    Calculate mean
    """

    if not list_number:
        raise ValueError("No elements")

    sort_elements = list(sorted(list_number))
    return sort_elements[int(len(sort_elements)/2)]

def percentil_fx(list_element, n_percentil):
    """
    Calculate percentil
    """

    if not list_element:
        raise ValueError("No elements")

    if len(list_element) == 1:
        return list_element[0]

    size = float(len(list_element))
    percentil = (float(n_percentil)/100.0) * (size-1)

    if int(percentil) == percentil:
        return list_element[int(percentil)]

    integer = math.floor(percentil)
    decimal = percentil - integer

    elx1 = list_element[integer]
    elx2 = list_element[integer+1]

    return elx1 + (decimal * (elx2 - elx1))

def quartil_fx(list_number):
    """
    Calculate Quantil
    """

    if not list_number:
        raise ValueError("No elements")

    internal_list = list(sorted(list_number))

    qua1 = percentil_fx(internal_list, 25)
    qua2 = percentil_fx(internal_list, 50)
    qua3 = percentil_fx(internal_list, 75)

    return (qua1, qua2, qua3)

def mean_fx(list_number):
    """
    Calculate mean
    """

    if not list_number:
        raise ValueError("No elements")

    sumatory = 0
    for number in list_number:
        sumatory += number

    return float(sumatory)/float(len(list_number))

def std_deviation_fx(list_number):
    """
    Calculate standard deviasion
    """

    if not list_number:
        raise ValueError("No elements")

    _mean = mean_fx(list_number)

    variance = 0
    for number in list_number:
        tmp = _mean - number
        tmp = tmp * tmp
        variance += tmp

    variance = variance/float(len(list_number))

    return math.sqrt(variance)


def requests_numbers():
    """
    Write a function that computes the standard deviation
    for a set of numbers coming from a list.
    Do not use any math module, compute the algorithm
    """

    _list_element = input("Insert numbers divided by spaces: ")

    try:
        _list_element = list(map(float, _list_element.split(" ")))

        median = median_fx(_list_element)
        print("The median is: {0}".format(median))

        mean = mean_fx(_list_element)
        print("The mean is: {0}".format(mean))

        std = std_deviation_fx(_list_element)
        print("The standard division is: {0}".format(std))

        quantilles = quartil_fx(_list_element)
        print("The Quantilles are: {0}".format(quantilles))

        n_percentil = input("Insert the n-percentil: ")
        v_percentil = percentil_fx(_list_element, n_percentil)
        print("The Percentil value is: {0}".format(v_percentil))

    except ValueError:
        print("Next time please insert a number")


if __name__ == "__main__":
    requests_numbers()
