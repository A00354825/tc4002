#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB1, Ex1

Date:
    February 2020

Main script
"""

import re

def part1():
    """
    Ask the user for a number.
    Depending on whether the number is even or odd,
    print out an appropriate message to the user.
    """

    print("Part1")

    grammar = "[\\+\\-]{0,1}[0-9]{1,}"
    num = str(input("Insert a number: "))

    if re.match(grammar, num) is None:
        print("the next time please insert a number")
    else:
        num = int(num)
        if num % 4 == 0:
            print("The number is divisible by 4")
        else:
            if num % 2 == 0:
                print("The number is even")
            else:
                print("The number is odd")

def part2():
    """
    Ask the user for two numbers:
        one number to check (call it num)
        one number to divide by (check).
    If check divides evenly into num, tell that to the user.
    If not, print a different appropriate message.
    """

    print("Part2")

    num = str(input("Insert a number: "))
    check = str(input("Insert a number: "))

    try:

        num = float(num)
        check = float(check)

        result = num/check

        if float(result) == int(result):
            print("Divides evenly into num")
        else:
            print("No divides evenly into num")

    except ZeroDivisionError:
        print("Zero is not a valid number of divide")

    except ValueError:
        print("Please insert a number the next time")


if __name__ == "__main__":

    part1()
    part2()
