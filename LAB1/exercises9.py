#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB1, Ex9

Date:
    February 2020

Main script
"""

RULES = {
    "M": 1000,
    "CM": 900,
    "D": 500,
    "CD": 400,
    "C": 100,
    "XC": 90,
    "L": 50,
    "XL": 40,
    "X": 10,
    "IX": 9,
    "V": 5,
    "IV": 4,
    "I": 1
}

def dec_to_rom(decimal):
    """
    Convert decimal number to roman number
    """

    if decimal >= 1000000:
        raise ArithmeticError

    if decimal >= 4000:

        million_part = int(str(decimal)[:-3])
        decimal_part = int(str(decimal)[-3:])

        million_part = dec_to_rom(million_part)
        million_part = list(map("*{}".format, list(million_part)))
        million_part = "".join(million_part)

        decimal_part = dec_to_rom(decimal_part)

        return million_part + decimal_part

    else:

        for letter, decimal_value in list(RULES.items()):
            if decimal >= decimal_value:
                return letter + dec_to_rom(decimal - decimal_value)

    return ""

def print_roman(roman):
    """
    Format a roman number
    if countains a million notation (*I) will transform
    For example the number 4001 will print:
    ***************
    __
    IVI
    ***************
    if you notice the importance is on the '_' up that mean million
    """

    million_count = 0
    new_roman = []

    for char in list(roman):
        if char == "*":
            million_count += 1
        else:
            new_roman.append(char)

    print("*"*20)

    if million_count:
        print("_"*million_count)

    print("".join(new_roman))
    print("*"*20)

def game():
    """
    Controller of roman
    """

    try:
        _dec = int(input("Insert a number: "))
        _roman = dec_to_rom(_dec)
        print("The roman number is:")
        print_roman(_roman)
    except ValueError:
        print("The next time insert a number")
    except ArithmeticError:
        print("Number too big to transform")


if __name__ == "__main__":
    game()
