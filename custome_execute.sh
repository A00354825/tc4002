#/bin/bash

mode=$1
regex=$2
options=("run" "coverage" "coverage_report" "pylint" "help" "cleanup" "doxygen")

if [ -z "$mode" ]; then
    echo "Missing mode [${options[@]}]"
    exit 1
fi

if [ "$mode" == "help" ]; then
    echo "Usage: $0 [${options[@]}] <file_regex>"
    exit 0
fi

if [[ ! " ${options[@]} " =~ " ${mode} " ]]; then
    echo "Invalid option: ${mode}"
    echo "Usage: $0 [${options[@]}] <file_regex>"
    exit 1
fi

if [ "$mode" == "cleanup" ]; then
    rm -rf coverage
    rm -rf tmpdata
    rm -f .coverage
    rm -rf html
    rm -rf latex
    rm -rf *.log
    exit 0
fi

if [ -z "$regex" ]; then
    regex=".py"
fi

if [ "$mode" == "doxygen" ]; then
    doxygen *$regex*
    exit 1
fi

files=( $(ls *.py | grep -v "__init__.py" | grep $regex) )

for file in "${files[@]}"; do
    base=`echo $file | awk -F. '{print $1}'`

    echo "*******************************"
    echo "***** Process File $file ******"

    if [ $mode == "run" ]; then
        python3 $file
    fi

    if [ $mode == "all" ] || [ $mode == "coverage" ] || [ $mode == "coverage_report" ]; then
        echo ""
        echo " *** Coverage ***"
        coverage run $file
    fi

    if [ $mode == "all" ] || [ $mode == "coverage_report" ]; then
        coverage html -d coverage/$base
        echo "Coverage report at: coverage/$base"
    fi

    if [ $mode == "all" ] || [ $mode == "coverage" ]; then
        mkdir -p coverage
        coverage report $file
    fi

    if [ $mode == "all" ] || [ $mode == "pylint" ]; then
        echo ""
        echo " *** Pylint ***"
        pylint3 $file 2>&1
    fi

    echo "*******************************"
done

