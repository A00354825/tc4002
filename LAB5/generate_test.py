#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
docstring
"""

import json
import sys
import random

def main():
    """
    Argument1: test_file
    Argument2: base file name for csvs
    Argument3: how many numbers
    """

    # Create numbers
    _numbers = []
    for _ in range(0, int(sys.argv[3])):
        _numbers.append(random.randint(0, 100000))

    # Dump csvs
    _filename = sys.argv[2]
    _input = "{0}.csv".format(_filename)
    with open(_input, "w") as _csv_file:
        _str = list(map(str, _numbers))
        _csv_file.write(", ".join(_str))

    _output = "{0}_result.csv".format(_filename)
    with open(_output, "w") as _csv_file:
        _str = list(map(str, list(sorted(_numbers))))
        _csv_file.write(", ".join(_str))

    # Update test
    _json = {}
    with open(sys.argv[1], "r") as _test_file:
        _json = json.load(_test_file)

    _json[_filename] = {}
    _json[_filename]["input_file"] = _input
    _json[_filename]["output_file"] = _output
    _json[_filename]["method"] = sys.argv[4]

    with open(sys.argv[1], "w") as _test_file:
        _test_file.write(json.dumps(_json, indent=4))


if __name__ == "__main__":
    main()
