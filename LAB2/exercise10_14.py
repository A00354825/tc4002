#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB2.2, ex10-14

Date:
    February 2020

Main script
"""

import os

class MyPowerList:
    """
    Class that provide a abstraction from list
    """

    def __init__(self):
        self.__elements = []

    def add_element(self, new_element):
        """ add """
        self.__elements.append(new_element)

    def remove_element(self, position):
        """ remove """
        try:
            self.__elements.pop(position)
        except IndexError:
            print("Element not found in position {0}".format(position))

    def get_elements(self):
        """ get """
        return self.__elements

    def get_elements_sorted(self):
        """ sort """

        sorted_list = list(self.__elements)
        lenght = len(sorted_list)

        for i in range(0, lenght):
            for j in range(0, lenght):
                if sorted_list[i] < sorted_list[j]:
                    tmp = sorted_list[i]
                    sorted_list[i] = sorted_list[j]
                    sorted_list[j] = tmp

        return sorted_list

    def left_merge(self, left_list):
        """ left_merge """
        self.__elements = left_list + self.__elements

    def right_merge(self, right_list):
        """ right_merge """
        self.__elements = self.__elements + right_list

    def save_to_file(self, filename):
        """ save file """
        with open(filename, "w") as _file:
            _file.write("\n".join(self.__elements))

    def read_from_file(self, filename):
        """ read file """
        try:
            with open(filename, "r") as _file:
                self.__elements = list(map(lambda x: x.strip(), _file.readlines()))
        except FileNotFoundError:
            print("File not found {0}".format(filename))



class Game:
    """
    Class manager of one MyPowerList
    """

    def __init__(self):
        self.__power = MyPowerList()
        self.__option = ""

    @staticmethod
    def clear_screen():
        """ Clear screen """
        if os.name == "nt":
            os.system("clear")
        else:
            os.system("tput reset")

    def option_menu(self):
        """ menu """

        print("Welcome to PowerList")
        print("Select one option")
        print("")
        print("A) Add Element")
        print("D) Delete Element")
        print("P) Print")
        print("PS) Print Sorted")
        print("R) Read from file")
        print("W) Write to file")
        print("MR) Merge a word list from the right")
        print("ML) merge a word list from the left")
        print("")
        print("Q) quit")

        self.__option = input("Insert your option: ").strip()

    def add_element(self):
        """ add """
        _new = input("Insert the element to add: ")
        self.__power.add_element(_new)
        self.print_power()

    def delete_element(self):
        """ delete """
        try:
            _pos = input("Insert the position of the element to delete: ")
            self.__power.remove_element(int(_pos))
        except ValueError:
            print("Next time please provide a number")
        self.print_power()

    def print_sorted(self):
        """ print """
        print(self.__power.get_elements_sorted())

    def print_power(self):
        """ print """
        print(self.__power.get_elements())

    def read_file(self):
        """ read file """
        _filename = input("Insert the filename: ")
        self.__power.read_from_file(_filename)
        self.print_power()

    def write_file(self):
        """ write file """
        _filename = input("Insert the filename: ")
        self.__power.save_to_file(_filename)
        print("File saved")

    def merge_words(self, order="left"):
        """ merge """
        _words = input("Insert the words: ").strip().split(" ")

        if order == "left":
            self.__power.left_merge(_words)
        else:
            self.__power.right_merge(_words)

        self.print_power()

    def play(self):
        """ interactive menu with options """

        while self.__option != "Q":

            self.option_menu()

            if self.__option == "A":
                self.add_element()
            elif self.__option == "D":
                self.delete_element()
            elif self.__option == "P":
                self.print_power()
            elif self.__option == "PS":
                self.print_sorted()
            elif self.__option == "R":
                self.read_file()
            elif self.__option == "W":
                self.write_file()
            elif self.__option == "MR":
                self.merge_words("right")
            elif self.__option == "ML":
                self.merge_words("left")
            elif self.__option == "Q":
                print("Bye")
            else:
                print("Insert a valid option")

            input("Press Enter to continue")
            Game.clear_screen()

if __name__ == "__main__":
    GAME = Game()
    GAME.play()
