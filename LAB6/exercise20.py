#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=broad-except

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB4.2

Date:
    March 2020

Main script
"""

import unittest
import time
import json
import uuid
import os

from ddt import ddt, file_data

class UserError(Exception):
    """ UserException """
    def __init__(self, message, errors):
        super(UserError, self).__init__(message)
        self.errors = errors

class UserManager:
    """
    User Manager to add users and find
    """

    def __init__(self, filename):
        self.__users = []
        self.__fields = ["uuid", "name", "email", "age", "country"]
        self.__filename = filename
        self.read_from_file(self.__filename)

    def new_user(self, user_dict):
        """ add new user """

        for field in self.__fields:
            if field not in list(user_dict.keys()):
                raise UserError("No name provided", field)

        self.__users.append(user_dict)
        self.save_to_file(self.__filename)

    def del_user(self, user_uuid):
        """ del user """

        if not user_uuid:
            return False

        filter_users = self.search_strict({"uuid": user_uuid})

        if filter_users:
            for user in filter_users:
                self.__users.remove(user)
            return True
        return False

    def search_strict(self, user_dict):
        """ Search by field with the exactly same elements """
        def compare_callback(user_dict_data, user_data):
            """ compare """
            return user_dict_data == user_data
        return self.search_user(user_dict, compare_callback)

    def search_user(self, user_dict, find_callback):
        """
        Search filter wrapper
        Recives:
            user_dict: dict with the fields of the users
            find_callback: compare_function between the provided and thelements
        """
        filter_users = []

        for user in self.__users:
            found = True
            for field, data in list(user_dict.items()):

                if data == "":
                    continue

                if not find_callback(user_dict[field], user[field]):
                    found = False
                    break

            if found:
                filter_users.append(user)

        return filter_users

    def save_to_file(self, filename):
        """ save file """
        with open(filename, "w") as _file:
            json.dump(self.__users, _file)

    def read_from_file(self, filename):
        """ read file """
        try:
            with open(filename, "r") as _file:
                self.__users = json.load(_file)
                return True
        except FileNotFoundError:
            return False


@ddt
class MyTestCase(unittest.TestCase):
    """ Test Class """

    def general_flow(self, user_stream, find_user, delete_user):
        """ general purpose test """

        os.system("mkdir -p tmpdata")
        _directory = "tmpdata/{0}".format(uuid.uuid1())
        _manager = UserManager(_directory)

        for _user in user_stream:
            _manager.new_user(_user)

        if find_user:
            _filter = _manager.search_strict(find_user["criteria"])
            self.assertEqual(_filter, find_user["result"])

        if delete_user is not None:
            _manager.del_user(delete_user)

        _manager.save_to_file(_directory)
        _manager.read_from_file(_directory)


    @file_data("resources/testdata_20_b.json")
    def test_boundary(self, user_stream, find_user, delete_user):
        """ boundary """
        self.general_flow(user_stream, find_user, delete_user)

    def test_inverse(self):
        """ invert """
        # No invert method to this class
        pass

    def test_cross(self):
        """ cross """
        # No public class with same methods
        pass

    @file_data("resources/testdata_20_e.json")
    def test_exception(self, user_stream, find_user, delete_user, exception):
        """ exception """

        exception_name = ""
        try:
            self.general_flow(user_stream, find_user, delete_user)
        except Exception as exp:
            exception_name = type(exp).__name__
        self.assertEqual(exception_name, exception)

    @file_data("resources/testdata_20_b.json")
    def test_performance(self, user_stream, find_user, delete_user):
        """ performance """

        _start_time = time.time()
        self.general_flow(user_stream, find_user, delete_user)
        _end_time = time.time()
        _total_time = _end_time - _start_time

        self.assertTrue(_total_time < 1)


if __name__ == "__main__":
    unittest.main()
