#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=broad-except

"""
Authors:
    Jonathan Elias Sandoval Talamantes

B
Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB5

Date:
    March 2020

Main script
"""

import unittest
import time
import os

from ddt import ddt, file_data

class SortWrapper:
    """ Class Doc """

    def __init__(self):
        self.__sort_method = "quick"
        self.__input_data = []
        self.__sorted_data = []
        self.__output_data = []
        self.__performance_data = {}

    def get_sort_method(self):
        """ getter """
        return self.__sort_method

    def set_sort_method(self, method):
        """ setter """
        self.__sort_method = method

    def get_input_data(self):
        """ getter """
        return self.__input_data

    def set_input_data(self, filename):
        """ setter """
        self.__input_data = SortWrapper.read_file_csv(filename)

    def get_output_data(self):
        """ getter """
        return self.__output_data

    def set_output_data(self, filename):
        """ setter """
        self.__output_data = SortWrapper.read_file_csv(filename)

    def get_sorted_data(self):
        """ getter """
        return self.__sorted_data

    def set_sorted_data(self, list_elements):
        """ setter """
        self.__sorted_data = list_elements

    def get_performance_data(self):
        """ getter """
        return self.__performance_data

    def set_performance_data(self, performance):
        """ setter """
        self.__performance_data = performance

    @staticmethod
    def read_file_csv(filename):
        """ read the csv file and returning as a single list """

        if not os.path.exists(filename):
            _msg = "File not found: {0}".format(filename)
            raise FileNotFoundError(_msg)

        _data = []

        with open(filename, "r") as _csvfile:
            _rows = _csvfile.readlines()
            for _row in _rows:
                _row_data = _row.split(",")
                _data += list(map(lambda x: int(x.strip()), _row_data))

        return _data

    @staticmethod
    def execute_merge_sort(list_elements):
        """ merge sort """

        def merge_bootstrap(array, left, right):
            """ merge bootstrap """

            if left < right:
                middle = int((left+(right-1))/2)
                merge_bootstrap(array, left, middle)
                merge_bootstrap(array, middle+1, right)
                merge_exec(array, left, middle, right)

        def merge_exec(array, left, middle, right):
            """ merge exec """

            left_cota = middle - left + 1
            right_cota = right - middle

            new_left = []
            new_right = []

            for i in range(0, left_cota):
                new_left.append(array[left + i])

            for j in range(0, right_cota):
                new_right.append(array[middle + 1 + j])

            i = 0
            j = 0
            k = left

            while i < left_cota and j < right_cota:
                if new_left[i] <= new_right[j]:
                    array[k] = new_left[i]
                    i += 1
                else:
                    array[k] = new_right[j]
                    j += 1
                k += 1

            while i < left_cota:
                array[k] = new_left[i]
                i += 1
                k += 1

            while j < right_cota:
                array[k] = new_right[j]
                j += 1
                k += 1

        _elements = list(list_elements)
        merge_bootstrap(_elements, 0, len(_elements)-1)
        return _elements

    @staticmethod
    def execute_native_sort(list_elements):
        """ merge sort """
        return list(sorted(list_elements))

    @staticmethod
    def execute_heap_sort(list_elements):
        """ heap sort """

        def heap_bootstrap(array):
            """ heap bootstrap """
            array_len = len(array)

            for i in range(array_len, -1, -1):
                heap_execute(array, array_len, i)

            for i in range(array_len-1, 0, -1):
                array[i], array[0] = array[0], array[i]
                heap_execute(array, i, 0)

        def heap_execute(array, array_len, position):
            """ heap execute """
            largest = position
            left = 2 * position + 1
            right = 2 * position + 2

            if left < array_len and array[position] < array[left]:
                largest = left

            if right < array_len and array[largest] < array[right]:
                largest = right

            if largest != position:
                tmp = array[position]
                array[position] = array[largest]
                array[largest] = tmp

                heap_execute(array, array_len, largest)

        _elements = list(list_elements)
        heap_bootstrap(_elements)
        return _elements

    @staticmethod
    def execute_quick_sort(list_elements):
        """ quick sort """

        def quick_bootstrap(array, start, end):
            """ quick sort bootstrap """
            if start >= end:
                return

            middle = partition(array, start, end)
            quick_bootstrap(array, start, middle-1)
            quick_bootstrap(array, middle+1, end)

        def partition(array, start, end):
            """ execute partition on quick sort """
            pivot = array[start]
            low = start + 1
            high = end

            while True:
                while low <= high and array[high] >= pivot:
                    high = high - 1

                while low <= high and array[low] <= pivot:
                    low = low + 1

                if low <= high:
                    array[low], array[high] = array[high], array[low]
                else:
                    break

            tmp = array[start]
            array[start] = array[high]
            array[high] = tmp

            return high

        _elements = list(list_elements)
        quick_bootstrap(_elements, 0, len(_elements)-1)
        return _elements

    def execute_sort(self):
        """ return the data sorted """

        # Init the data to sort
        _method = self.get_sort_method()
        _input = self.get_input_data()
        _output = None

        # execute the sort wrapper
        _start_time = time.time()

        if _method == "quick":
            _output = SortWrapper.execute_quick_sort(_input)
        elif _method == "heap":
            _output = SortWrapper.execute_heap_sort(_input)
        elif _method == "merge":
            _output = SortWrapper.execute_merge_sort(_input)
        elif _method == "native":
            _output = SortWrapper.execute_native_sort(_input)
        else:
            _msg = "Method not implemented: {0}".format(_method)
            raise NotImplementedError(_msg)

        self.set_sorted_data(_output)

        _end_time = time.time()
        _total_time = _start_time - _end_time

        # Set performance metrics
        _performance = {}
        _performance["start_time"] = _start_time
        _performance["end_time"] = _end_time
        _performance["number_records"] = len(_output)
        _performance["total_time"] = _total_time

        self.set_performance_data(_performance)

@ddt
class MyTestCase(unittest.TestCase):
    """ Test Class """

    @file_data("resources/testdata_25_b.json")
    def test_boundary(self, input_file, output_file, method):
        """ boundary """

        _sort_wrapper = SortWrapper()
        _sort_wrapper.set_sort_method(method)
        _sort_wrapper.set_input_data(input_file)
        _sort_wrapper.set_output_data(output_file)
        _sort_wrapper.execute_sort()

        _output = _sort_wrapper.get_sorted_data()
        _expected = _sort_wrapper.get_output_data()

        self.assertEqual(_output, _expected)

    def test_inverse(self):
        """ inverse """
        # No inverse method to sort since sort is not reversible
        pass

    @file_data("resources/testdata_25_b.json")
    def test_cross(self, input_file, output_file, method):
        """ cross """

        _sort_wrapper1 = SortWrapper()
        _sort_wrapper1.set_sort_method(method)
        _sort_wrapper1.set_input_data(input_file)
        _sort_wrapper1.set_output_data(output_file)
        _sort_wrapper1.execute_sort()

        _sort_wrapper2 = SortWrapper()
        _sort_wrapper2.set_sort_method("native")
        _sort_wrapper2.set_input_data(input_file)
        _sort_wrapper2.set_output_data(output_file)
        _sort_wrapper2.execute_sort()

        _output1 = _sort_wrapper1.get_sorted_data()
        _expected1 = _sort_wrapper1.get_output_data()

        _output2 = _sort_wrapper2.get_sorted_data()
        _expected2 = _sort_wrapper2.get_output_data()

        self.assertEqual(_output1, _expected1)
        self.assertEqual(_output2, _expected2)
        self.assertEqual(_output1, _output2)

    @file_data("resources/testdata_25_e.json")
    def test_exception(self, input_file, output_file, method, exception):
        """ exception """

        _exception_name = ""

        try:

            _sort_wrapper = SortWrapper()
            _sort_wrapper.set_sort_method(method)
            _sort_wrapper.set_input_data(input_file)
            _sort_wrapper.set_output_data(output_file)
            _sort_wrapper.execute_sort()

        except Exception as exp:
            _exception_name = type(exp).__name__
        self.assertEqual(_exception_name, exception)

    @file_data("resources/testdata_25_b.json")
    def test_performance(self, input_file, output_file, method):
        """ performance """

        _sort_wrapper = SortWrapper()
        _sort_wrapper.set_sort_method(method)
        _sort_wrapper.set_input_data(input_file)
        _sort_wrapper.set_output_data(output_file)
        _sort_wrapper.execute_sort()

        _performance = _sort_wrapper.get_performance_data()
        self.assertLessEqual(_performance["total_time"], 10)

if __name__ == "__main__":
    unittest.main()
