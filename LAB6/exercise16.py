#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=broad-except

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB4.2

Date:
    March 2020

Main script
"""

import unittest
import math
import time
from ddt import ddt, file_data

@ddt
class MyTestCase(unittest.TestCase):
    """ Test Class """

    @staticmethod
    def ceil(num):
        """ own math.ceil function """
        res = int(num)
        if res == num or num < 0:
            return res
        return res + 1

    @file_data("resources/testdata_16_b.json")
    def test_boundary(self, input_value, output_value):
        """ Test boundary """
        _res = math.ceil(input_value)
        self.assertEqual(_res, output_value)

    @file_data("resources/testdata_16_b.json")
    def test_inverse(self, input_value, output_value):
        """ Test inverse """
        _res1 = math.ceil(input_value)
        _res2 = math.ceil(output_value)
        self.assertEqual(_res1, _res2)

    @file_data("resources/testdata_16_b.json")
    def test_cross_check(self, input_value, output_value):
        """ Test cross check """
        _res1 = math.ceil(input_value)
        _res2 = MyTestCase.ceil(input_value)
        self.assertEqual(_res1, _res2)

        _res1 = math.ceil(output_value)
        _res2 = MyTestCase.ceil(output_value)
        self.assertEqual(_res1, _res2)

    @file_data("resources/testdata_16_e.json")
    def test_error_condition(self, input_value, exception_name):
        """ Test error """

        try:
            math.ceil(input_value)
            raise Exception
        except Exception as exp:
            self.assertEqual(type(exp).__name__, exception_name)

    @file_data("resources/testdata_16_b.json")
    def test_performance(self, input_value, output_value):
        """ test performance """
        _start_time = time.time()
        math.ceil(input_value)
        math.ceil(output_value)
        _end_time = time.time()

        _total_time = _end_time - _start_time
        self.assertTrue(_total_time < 1)


if __name__ == "__main__":
    unittest.main()
