#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=broad-except

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB4.2

Date:
    March 2020

Main script
"""

import math
import unittest
import time

import numpy
from ddt import ddt, file_data

class Calculator():
    """ Static Wrapper """

    @staticmethod
    def median_fx(list_number):
        """
        Calculate mean
        """

        if not list_number:
            raise ValueError("No elements")

        sort_elements = list(sorted(list_number))
        return sort_elements[int(len(sort_elements)/2)]

    @staticmethod
    def percentil_fx(list_element, n_percentil):
        """
        Calculate percentil
        """

        if not list_element:
            raise ValueError("No elements")

        if len(list_element) == 1:
            return list_element[0]

        size = float(len(list_element))
        percentil = (float(n_percentil)/100.0) * (size-1)

        if int(percentil) == percentil:
            return list_element[int(percentil)]

        integer = math.floor(percentil)
        decimal = percentil - integer

        elx1 = list_element[integer]
        elx2 = list_element[integer+1]

        return elx1 + (decimal * (elx2 - elx1))

    @staticmethod
    def quartil_fx(list_number):
        """
        Calculate Quantil
        """

        if not list_number:
            raise ValueError("No elements")

        internal_list = list(sorted(list_number))

        qua1 = Calculator.percentil_fx(internal_list, 25)
        qua2 = Calculator.percentil_fx(internal_list, 50)
        qua3 = Calculator.percentil_fx(internal_list, 75)

        return (qua1, qua2, qua3)

    @staticmethod
    def mean_fx(list_number):
        """
        Calculate mean
        """

        if not list_number:
            raise ValueError("No elements")

        sumatory = 0
        for number in list_number:
            sumatory += number

        return float(sumatory)/float(len(list_number))

    @staticmethod
    def std_deviation_fx(list_number):
        """
        Calculate standard deviasion
        """

        if not list_number:
            raise ValueError("No elements")

        _mean = Calculator.mean_fx(list_number)

        variance = 0
        for number in list_number:
            tmp = _mean - number
            tmp = tmp * tmp
            variance += tmp

        variance = variance/float(len(list_number))

        return math.sqrt(variance)

@ddt
class MyTestCase(unittest.TestCase):
    """ Test Class """

    def cross_test(self, fx_name, numbers, ratio, expected_result):
        """ general purpose test """

        _result = None

        if fx_name == "mean":
            _result = numpy.mean(numbers)

        elif fx_name == "std_deviation":
            _result = numpy.std(numbers)

        elif fx_name == "median":
            _result = numpy.median(numbers)

        elif fx_name == "quantil":
            if ratio == 0:
                _result = numpy.percentile(numbers, 0.25)

            elif ratio == 1:
                _result = numpy.percentile(numbers, 0.5)

            elif ratio == 2:
                _result = numpy.percentile(numbers, 0.75)

        elif fx_name == "percentil":
            _result = numpy.percentile(numbers, ratio)

        self.assertAlmostEqual(_result, expected_result, places=0)

    def general_test(self, fx_name, numbers, ratio, expected_result):
        """ general purpose test """

        _result = None

        if fx_name == "mean":
            _result = Calculator.mean_fx(numbers)

        elif fx_name == "std_deviation":
            _result = Calculator.std_deviation_fx(numbers)

        elif fx_name == "median":
            _result = Calculator.median_fx(numbers)

        elif fx_name == "quantil":
            _r1, _r2, _r3 = Calculator.quartil_fx(numbers)

            if ratio == 0:
                _result = _r1
            elif ratio == 1:
                _result = _r2
            elif ratio == 2:
                _result = _r3

        elif fx_name == "percentil":
            _result = Calculator.percentil_fx(numbers, ratio)

        self.assertAlmostEqual(_result, expected_result, places=0)

    @file_data("resources/testdata_08_b.json")
    def test_boundary(self, fx_name, numbers, ratio, expected_result):
        """ boundary """
        self.general_test(fx_name, numbers, ratio, expected_result)

    def test_inverse(self):
        """ invert """
        # No invert method to this class
        pass

    @file_data("resources/testdata_08_b.json")
    def test_cross(self, fx_name, numbers, ratio, expected_result):
        """ cross """
        self.cross_test(fx_name, numbers, ratio, expected_result)

    @file_data("resources/testdata_08_e.json")
    def test_exception(self, fx_name, numbers, ratio, exception):
        """ exception """

        exception_name = ""
        try:
            self.general_test(fx_name, numbers, ratio, 0)
        except Exception as exp:
            exception_name = type(exp).__name__
        self.assertEqual(exception_name, exception)

    @file_data("resources/testdata_08_b.json")
    def test_performance(self, fx_name, numbers, ratio, expected_result):
        """ performance """

        _start_time = time.time()
        self.general_test(fx_name, numbers, ratio, expected_result)
        _end_time = time.time()
        _total_time = _end_time - _start_time

        self.assertTrue(_total_time < 1)


if __name__ == "__main__":
    unittest.main()
