#!/usr/bin/python
# -*- coding: utf-8 -*-
# pylint: disable=broad-except,too-many-arguments

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB4.2

Date:
    March 2020

Main script
"""

import unittest
import filecmp
import time
import os
from ddt import ddt, file_data

@ddt
class MyTestCase(unittest.TestCase):
    """ Test Class """

    @staticmethod
    def cmp(file1, file2):
        """ cmp """
        return MyTestCase.inverse_cmp(file2, file1)

    @staticmethod
    def inverse_cmp(file1, file2):
        """ inverse cmp """
        if os.stat(file1) == os.stat(file2):
            return True

        _result = False
        with open(file1, "r") as _f1:
            with open(file2, "r") as _f2:
                _result = _f1.read() == _f2.read()

        return  _result

    @file_data("resources/testdata_19_b.json")
    def test_boundary(self, precmd, file1, file2, postcmd, result):
        """ Test boundary """
        os.system(precmd)
        _res = filecmp.cmp(file1, file2, shallow=False)
        os.system(postcmd)

        self.assertEqual(_res, result)

    @file_data("resources/testdata_19_b.json")
    def test_inverse(self, precmd, file1, file2, postcmd, result):
        """ Test inverse """
        os.system(precmd)
        _res1 = filecmp.cmp(file1, file2, shallow=False)
        _res2 = MyTestCase.inverse_cmp(file1, file2)
        os.system(postcmd)

        self.assertEqual(_res1, result)
        self.assertEqual(_res2, result)

    @file_data("resources/testdata_19_b.json")
    def test_cross_check(self, precmd, file1, file2, postcmd, result):
        """ Test cross check """
        os.system(precmd)
        _res1 = filecmp.cmp(file1, file2, shallow=False)
        _res2 = MyTestCase.cmp(file1, file2)
        os.system(postcmd)

        self.assertEqual(_res1, result)
        self.assertEqual(_res2, result)

    @file_data("resources/testdata_19_e.json")
    def test_error_condition(self, file1, file2, exception_name):
        """ Test error """
        try:
            _res1 = filecmp.cmp(file1, file2)
            raise Exception
        except Exception as exp:
            self.assertEqual(type(exp).__name__, exception_name)

    @file_data("resources/testdata_19_b.json")
    def test_performance(self, precmd, file1, file2, postcmd, result):
        """ test performance """
        _start_time = time.time()

        os.system(precmd)
        _res = filecmp.cmp(file1, file2, shallow=False)
        os.system(postcmd)
        self.assertEqual(_res, result)

        _end_time = time.time()
        _total_time = _end_time - _start_time

        self.assertTrue(_total_time < 1)

if __name__ == "__main__":
    unittest.main()
