#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
Authors:
    Jonathan Elias Sandoval Talamantes

Tec Number:
    A00354825

Subject:
    Software analysis, design and construction

Activity:
    LAB2.2, ex15

Date:
    February 2020

Main script
"""

import json
import re
import os

import logging
from logging.config import fileConfig

class UserError(Exception):
    """ UserException """
    def __init__(self, message, errors):
        super(UserError, self).__init__(message)
        self.errors = errors

class UserManager:
    """
    User Manager to add users and find
    """

    def __init__(self):
        self.__users = []
        self.__fields = ["name", "address", "phone", "email"]

        fileConfig('logging_config.conf')
        self.__logger = logging.getLogger("ex15")

    def get_logger(self):
        """ getter """
        return self.__logger

    def new_user(self, user_dict):
        """ add new user """

        self.get_logger().info("Calling new_user with: %s", user_dict)

        for field in self.__fields:
            if field not in list(user_dict.keys()):
                raise UserError("No name provided", field)

        self.__users.append(user_dict)

    def search_strict(self, user_dict):
        """ Search by field with the exactly same elements """

        self.get_logger().info("Calling search_strict with: %s", user_dict)

        def compare_callback(user_dict_data, user_data):
            """ compare """
            return user_dict_data == user_data
        return self.search_user(user_dict, compare_callback)

    def search_like(self, user_dict):
        """ Search by field that start with the data """

        self.get_logger().info("Calling search_like with: %s", user_dict)

        def compare_callback(user_dict_data, user_data):
            """ compare """
            return user_data.startswith(user_dict_data)
        return self.search_user(user_dict, compare_callback)

    def search_regex(self, user_dict):
        """ Search by field that start with regex """

        self.get_logger().info("Calling search_regex with: %s", user_dict)

        def compare_callback(user_dict_regex, user_data):
            """ compare """
            _pat = re.match(user_dict_regex, user_data)
            return _pat is not None
        return self.search_user(user_dict, compare_callback)

    def search_user(self, user_dict, find_callback):
        """
        Search filter wrapper
        Recives:
            user_dict: dict with the fields of the users
            find_callback: compare_function between the provided and thelements
        """
        filter_users = []

        for user in self.__users:
            found = True
            for field, data in list(user_dict.items()):

                if data == "":
                    continue

                if not find_callback(user_dict[field], user[field]):
                    found = False
                    break

            if found:
                filter_users.append(user)

        self.get_logger().info("Search User result are %s", filter_users)

        return filter_users

    def save_to_file(self, filename):
        """ save file """

        self.get_logger().info("Save progress on file %s", filename)

        with open(filename, "w") as _file:
            json.dump(self.__users, _file)

    def read_from_file(self, filename):
        """ read file """
        try:
            self.get_logger().info("Read progress from file %s", filename)
            with open(filename, "r") as _file:
                self.__users = json.load(_file)
        except FileNotFoundError:
            self.__logger.error("File not found: %s", filename)

class Game:
    """ Game """

    def __init__(self):
        self.__users = UserManager()
        self.__option = ""

    @staticmethod
    def print_json(json_obj):
        """ print json """
        print(json.dumps(json_obj, indent=4))

    @staticmethod
    def clear_screen():
        """ Clear screen """
        if os.name == "nt":
            os.system("clear")
        else:
            os.system("tput reset")

    def option_menu(self):
        """ menu """

        self.__users.get_logger().info("Welcome to User Filter")
        self.__users.get_logger().info("Select one option")
        self.__users.get_logger().info("")
        self.__users.get_logger().info("A) Add Element")
        self.__users.get_logger().info("P) Print")
        self.__users.get_logger().info("R) Read from file")
        self.__users.get_logger().info("W) Write to file")
        self.__users.get_logger().info("F1) Find by exact string")
        self.__users.get_logger().info("F2) Find by initial string")
        self.__users.get_logger().info("F3) Find by regex string")
        self.__users.get_logger().info("")
        self.__users.get_logger().info("Q) quit")

        self.__option = input("Insert your option: ").strip()
        self.__users.get_logger().info("Choosen option: %s", self.__option)

    def print_all(self):
        """ print """
        _filter = {"name": ".*"}
        Game.print_json(self.__users.search_regex(_filter))

    def add_element(self):
        """ add """
        self.__users.get_logger().info("Insert New Element")

        _new = {}
        _new["name"] = input("Insert the name: ").strip()
        _new["address"] = input("Insert the address: ").strip()
        _new["phone"] = input("Insert the phone: ").strip()
        _new["email"] = input("Insert the email: ").strip()

        self.__users.new_user(_new)
        self.print_all()

    def read_file(self):
        """ read file """
        _filename = input("Insert the filename: ")
        self.__users.read_from_file(_filename)
        self.print_all()

    def write_file(self):
        """ write file """
        _filename = input("Insert the filename: ")
        self.__users.save_to_file(_filename)
        self.__users.get_logger().info("File saved")

    def search_by(self, type_search="strict"):
        """ search """

        self.__users.get_logger().info("Apply the %s search", type_search)
        self.__users.get_logger().info("if nothing inserted, assume not search by that field")

        _new = {}
        _new["name"] = input("Insert the name: ").strip()
        _new["address"] = input("Insert the address: ").strip()
        _new["phone"] = input("Insert the phone: ").strip()
        _new["email"] = input("Insert the email: ").strip()

        if type_search == "strict":
            Game.print_json(self.__users.search_strict(_new))
        elif type_search == "like":
            Game.print_json(self.__users.search_like(_new))
        elif type_search == "regex":
            Game.print_json(self.__users.search_regex(_new))
        else:
            self.__users.get_logger().info("No type found: %s", type_search)

    def play(self):
        """ interactive menu with options """

        while self.__option != "Q":

            self.option_menu()

            if self.__option == "A":
                self.add_element()
            elif self.__option == "P":
                self.print_all()
            elif self.__option == "R":
                self.read_file()
            elif self.__option == "W":
                self.write_file()
            elif self.__option == "F1":
                self.search_by("strict")
            elif self.__option == "F2":
                self.search_by("like")
            elif self.__option == "F3":
                self.search_by("regex")
            elif self.__option == "Q":
                self.__users.get_logger().info("Bye")
            else:
                self.__users.get_logger().info("Insert a valid option")

            input("Press Enter to continue")
            Game.clear_screen()

if __name__ == "__main__":
    GAME = Game()
    GAME.play()
